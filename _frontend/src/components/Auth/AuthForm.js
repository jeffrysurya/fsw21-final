import { useState, useRef, useContext } from "react";
import { useHistory } from "react-router-dom";
import { addDoc, collection } from "firebase/firestore";
import { db } from "../../firebase-config";

import AuthContext from "../../store/auth-context";
import classes from "./AuthForm.module.css";

const AuthForm = () => {
  const history = useHistory();
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  const uname = useRef();
  const [gender, setGender] = useState(false);

  const authCtx = useContext(AuthContext);

  const [isLogin, setIsLogin] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };

  const submitHandler = (event) => {
    event.preventDefault();

    const enteredEmail = emailInputRef?.current?.value;
    const enteredPassword = passwordInputRef?.current?.value;
    const enteredUname = uname?.current?.value;

    setIsLoading(true);
    let url;
    if (isLogin) {
      url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBzieYn4WxauOKpV-WIBSW8PW3S39xRkPE";
    } else {
      url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBzieYn4WxauOKpV-WIBSW8PW3S39xRkPE";
    }

    fetch(url, {
      method: "POST",
      body: JSON.stringify({
        email: enteredEmail,
        password: enteredPassword,
        returnSecureToken: true,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        setIsLoading(false);
        if (res.ok) {
          return res.json();
        } else {
          return res.json().then((data) => {
            let errorMessage = "Authentication failed!";
            // if (data && data.error && data.error.message) {
            //   errorMessage = data.error.message;
            // }

            throw new Error(errorMessage);
          });
        }
      })
      .then((data) => {
        const expirationTime = new Date(
          new Date().getTime() + +data.expiresIn * 1000
        );
        authCtx.login(data.idToken, expirationTime.toISOString());
        history.replace("/dashboard");
      })
      .then((data) => {
        if (!isLogin) {
          const user = collection(db, "users");
          addDoc(user, { uname: enteredUname, gender: gender }).then((res) =>
            console.warn(res.id)
          );
        }
      })
      .catch((err) => {
        alert(err.message);
      });
  };

  return (
    <section className={classes.auth}>
      <h1>{isLogin ? "Login" : "Sign Up"}</h1>
      <form onSubmit={submitHandler}>
        {!isLogin ? (
          <>
            {" "}
            <div className={classes.control}>
              <label htmlFor="uname">Username</label>
              <input type="text" id="uname" required ref={uname} />
            </div>
            <div className={classes.control}>
              <div className={classes.radio}>
                <input
                  type="radio"
                  name="pria"
                  value={gender}
                  checked={gender}
                  onChange={() => setGender(!gender)}
                />{" "}
                <p>Pria</p>
              </div>
              <div className={classes.radio}>
                <input
                  type="radio"
                  name="wanita"
                  value={gender === false}
                  checked={gender === false}
                  onChange={() => setGender(!gender)}
                />{" "}
                <p>Wanita</p> 
              </div>
            </div>
          </>
        ) : null}

        <div className={classes.control}>
          <label htmlFor="email">Your Email</label>
          <input type="email" id="email" required ref={emailInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="password">Your Password</label>
          <input
            type="password"
            id="password"
            required
            ref={passwordInputRef}
          />
        </div>
        <div className={classes.actions}>
          {!isLoading && (
            <button>{isLogin ? "Login" : "Create Account"}</button>
          )}
          {isLoading && <p>Sending request...</p>}
          <button
            type="button"
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? "Create new account" : "Login with existing account"}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
