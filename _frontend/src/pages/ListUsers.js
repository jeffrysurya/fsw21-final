import { createUserWithEmailAndPassword, getAuth } from "firebase/auth";
import { collection, getDocs } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../firebase-config";

function ListUsers() {
  // List batch of users, 1000 at a time.
  const [allUser, setAllUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  function getUsers() {
    const user = collection(db, "users");
    getDocs(user)
      .then((response) => {
        const user = response.docs.map((doc) => ({
          data: doc.data(),
          id: doc.id,
        }));
        setAllUsers(user);
      })
      .catch((err) => console.warn(err.message));
  }

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>User List</h1>
      <ul
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        {allUser.map((el) => (
          <div
            style={{
              display: "flex",
              listStyleType: "none",
              padding: "10px 20px",
              border: "1px solid #111",
              width: "20%",
            }}
          >
            <li
              style={{
                width: "120px",
              }}
            >
              {el.data.uname}
            </li>
            <li>{el.data.gender ? "pria" : "Wanita"}</li>
          </div>
        ))}
      </ul>
      {/* <div>{allUser.map((el) => el.data.uname)}</div>
      <div>{allUser.map((el) => (el.data.gender ? "pria" : "Wanita"))}</div> */}
    </div>
  );
}

export default ListUsers;
