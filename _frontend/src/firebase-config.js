import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBzieYn4WxauOKpV-WIBSW8PW3S39xRkPE",
  authDomain: "chapter-9-567a0.firebaseapp.com",
  databaseURL:
    "https://chapter-9-567a0-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "chapter-9-567a0",
  storageBucket: "chapter-9-567a0.appspot.com",
  messagingSenderId: "439688786020",
  appId: "1:439688786020:web:3770e1f1625dbffd3a6f71",
  measurementId: "G-B59HW4WNGB",
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const db = getFirestore(app);
